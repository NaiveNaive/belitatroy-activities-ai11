 package com.example.activity3;

 import android.app.AlertDialog;
 import android.os.Bundle;
 import android.widget.Button;
 import android.widget.EditText;

 import androidx.appcompat.app.AppCompatActivity;

 public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final EditText Input = (EditText) findViewById( R.id.et_input );
        Button Show = (Button) findViewById( R.id.bt_show );

        Show.setOnClickListener(v -> {
            String Text = Input.getText().toString();
            if (Text.isEmpty()){
                alert( "Please Insert Data !!!" );
            }else {
                alert(Text);
        }

    });
 }
     private void alert(String message) {
         AlertDialog dlg = new AlertDialog.Builder(MainActivity.this)
                 .setTitle( "Message" )
                 .setMessage( message )
                 .setPositiveButton( "OK", (dialog, which) -> dialog.dismiss())
                 .create();
         dlg.show();
     }
 }
